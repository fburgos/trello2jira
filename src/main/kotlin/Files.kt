import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.WildcardFileFilter
import java.io.File
import java.io.InputStream


fun getResourceAsText(path: String): String = object {}.javaClass.getResource(path).readText(charset = Charsets.UTF_8)

fun getAllFilesThatMatchExtension(directory: File, extension: String): Collection<File> =
    FileUtils.listFiles(directory, WildcardFileFilter(extension), null)

fun File.copyInputStreamToFile(inputStream: InputStream) {
    this.outputStream().use { fileOut ->
        inputStream.copyTo(fileOut)
    }
}