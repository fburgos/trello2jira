import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import commands.Authors
import commands.Transform

class Trello2Jira : CliktCommand() {
    override fun run() = Unit
}

fun main(args : Array<String>) {
    Trello2Jira().subcommands(Authors(), Transform()).main(args)
}

