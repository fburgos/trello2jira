package domain.models

import java.time.LocalDateTime


data class JiraUser(val name: String? = null, val fullname: String)
data class JiraLink(val name: String? = null, val sourceId: String? = null, val destinationId: String? = null)
data class JiraVersion(val name: String? = null, val released: Boolean? = null, val releaseDated: LocalDateTime? = null)
data class JiraHistoryItem(
    val fieldType: String? = null,
    val field: String? = null,
    val from: String? = null,
    val fromString: String? = null,
    val to: String? = null,
    val toString: String? = null
)

data class JiraHistoryEntry(
    val author: String? = null,
    val created: LocalDateTime? = null,
    val items: Array<JiraHistoryItem>? = null
)

data class JiraCustomField(val name: String? = null, val fieldType: String? = null, val value: String)

data class JiraAttachment(
    val name: String? = null,
    val attacher: String? = null,
    val created: LocalDateTime? = null,
    val uri: String? = null,
    val description: String? = null
)

data class JiraComment(val body: String? = null, val author: String? = null, val created: LocalDateTime? = null)

data class JiraIssueLink(val name: String? = null, val issue: JiraIssue? = null)

data class JiraIssue(
    val priority: String? = null,
    val description: String? = null,
    val status: String? = null,
    val reporter: String? = null,
    val labels: Collection<String>? = null,
    val watchers: Array<String>? = null,
    val issueType: String? = null,
    val resolution: String? = null,
    val created: LocalDateTime? = null,
    val updated: LocalDateTime? = null,
    val affectedVersions: Array<String>? = null,
    val summary: String? = null,
    val assignee: String? = null,
    val components: Array<String>? = null,
    val externalId: String? = null,
    val history: Array<JiraHistoryEntry>? = null,
    val customFieldsValues: Array<JiraCustomField>? = null,
    val attachments: Collection<JiraAttachment>? = null,
    val comments: Collection<JiraComment>? = null,
    @Transient val links: Collection<JiraIssueLink>? = null
)

data class JiraProject(
    val name: String? = null,
    val key: String? = null,
    val type: String? = null,
    val description: String? = null,
    val versions: Array<JiraVersion>? = null,
    val components: Array<String>? = null,
    val issues: Collection<JiraIssue>? = null
)

data class Jira(
    val users: Collection<JiraUser>? = null,
    val links: Collection<JiraLink>? = null,
    val projects: Collection<JiraProject>? = null
)