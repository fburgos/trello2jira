package domain.models

import java.time.ZonedDateTime

/**
 * Represent an object with Id.
 * Two objects are equal if they have equals ids. Other properties does not matter.
 */
abstract class Entity {
    abstract val id: String?
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Entity

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}

data class TrelloList(
    override val id: String? = null,
    val name: String? = null,
    val closed: Boolean? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloMember(
    override val id: String? = null,
    val username: String? = null,
    val fullName: String? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloAttachment(
    override val id: String? = null,
    val date: ZonedDateTime? = null,
    val member: TrelloMember? = null,
    val isUpload: Boolean? = null,
    val name: String? = null,
    val url: String
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloLabel(override val id: String? = null, val name: String? = null) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloActionData(val text: String? = null, val cardId: String? = null)

data class TrelloAction(
    override val id: String? = null,
    val creator: TrelloMember? = null,
    val data: TrelloActionData? = null,
    val type: String? = null,
    val date: ZonedDateTime? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloChecklistItem(
    override val id: String? = null,
    val state: String? = null,
    val name: String? = null
): Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloChecklist(
    override val id: String? = null,
    val name: String? = null,
    val idCard: String? = null,
    val checkItems: Collection<TrelloChecklistItem>? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloCard(
    override val id: String? = null,
    val closed: Boolean? = null,
    val dateLastActivity: ZonedDateTime? = null,
    val desc: String? = null,
    val list: TrelloList? = null,
    val name: String? = null,
    val dueComplete: Boolean? = null,
    val due: ZonedDateTime? = null,
    val shortUrl: String? = null,
    val attachments: Collection<TrelloAttachment>? = null,
    val idChecklists: Collection<String>? = null,
    val members: Collection<TrelloMember>? = null,
    val labels: Collection<TrelloLabel>? = null,
    val actions: Collection<TrelloAction>? = null,
    val checklists: Collection<TrelloChecklist>? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}

data class TrelloBoard(
    override val id: String? = null,
    val name: String? = null,
    val desc: String? = null,
    val shortUrl: String? = null,
    val dateLastActivity: ZonedDateTime? = null,
    val labelNames: HashMap<String, String>? = null,
    val cards: Collection<TrelloCard>? = null,
    val lists: Collection<TrelloList>? = null,
    val members: Collection<TrelloMember>? = null,
    val actions: Collection<TrelloAction>? = null,
    val checklists: Collection<TrelloChecklist>? = null
) : Entity() {
    override fun equals(other: Any?) = super.equals(other)
    override fun hashCode() = super.hashCode()
}
