package commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import com.github.ajalt.clikt.parameters.types.file
import com.google.gson.*
import com.google.gson.stream.JsonReader
import deserializers.TrelloBoardJsonDeserializer
import domain.models.*
import getAllFilesThatMatchExtension
import serializers.LocalDateTimeStringSerializer
import java.io.File
import java.io.FileReader
import java.time.LocalDateTime
import java.util.stream.Collectors
import java.util.stream.Stream

// TODO This should throw an error if userMapping does not exist or set the same user as default
fun TrelloAttachment.toJiraAttachment(usersMapping: HashMap<String, String>) =
    JiraAttachment(name = name, uri = url, created = date?.toLocalDateTime(), attacher = usersMapping[member?.username])

fun TrelloAction.toJiraComment(usersMapping: HashMap<String, String>) =
    JiraComment(body = data?.text, created = date?.toLocalDateTime(), author = usersMapping[creator?.username])

/**
 * Jira JSON importer not support weblinks so this generate a markdown string in order to append it in the
 * description or other field.
 */
fun TrelloAttachment.toJiraWeblink() = "[$name|$url]"

fun TrelloCard.toJiraIssue(mapping: DataMapping): JiraIssue {
    val lbls = labels?.map { it.name!! }?.filter { it.trim().isNotEmpty() }
    var issType = "tasks"
    if (lbls != null) {
        val iterator = mapping.labelsTypes.keys.intersect(lbls).iterator()
        if (iterator.hasNext()) {
            issType = mapping.labelsTypes[iterator.next()]!!
        }
    }

    val webLinks: String? =
        attachments?.filter { it.isUpload == false }?.takeIf { it.isNotEmpty() }?.map { it.toJiraWeblink() }
            ?.reduce { s, t -> "$s\n$t" }

    val d = if (webLinks != null) "$desc\n\n$webLinks" else desc
    return JiraIssue(
        externalId = id,
        summary = name,
        description = d,
        labels = lbls,
        issueType = issType,
        status = if (closed!!) mapping.archiveStatus else mapping.listStatus[list?.name],
        updated = dateLastActivity?.toLocalDateTime(),
        attachments = attachments?.filter { it.isUpload == true }?.map { it.toJiraAttachment(mapping.users) },
        comments = actions?.map { it.toJiraComment(mapping.users) }
    )
}

fun TrelloChecklist.toJiraIssuesSubtask(
    subTaskIssueType: String,
    stateStatusMapping: HashMap<String, String>,
    links: Collection<JiraIssueLink>
) = checkItems?.map {
    JiraIssue(
        summary = it.name,
        status = stateStatusMapping[it.state],
        links = links,
        issueType = subTaskIssueType,
        externalId = it.id
    )
}

fun TrelloBoard.toJira(mapping: DataMapping): Jira {
    val project = toJiraProject(mapping)
    val links = project.issues?.flatMap { iss -> iss.links?.map { iss to it } ?: listOf() }
        ?.map { JiraLink(name = it.second.name, sourceId = it.first.externalId, destinationId = it.second.issue?.externalId) }

    return Jira(
        projects = listOf(project),
        links = links
    )
}

fun TrelloBoard.toJiraProject(mapping: DataMapping): JiraProject {
    val issues = cards?.parallelStream()?.map { c -> c to  c.toJiraIssue(mapping) }?.
    flatMap {
        // Checklist -> IssueSubTask(Parent)
        val subtasks = it.first.checklists?.parallelStream()?.flatMap { chkls ->
            chkls.toJiraIssuesSubtask(
                "Sub-task",
                mapping.stateStatus,
                listOf(JiraIssueLink("sub-task-link", it.second))
            )?.stream()
        }

        val parent = Stream.of(it.second)
        if (subtasks != null) Stream.concat(parent, subtasks) else parent
    }?.collect(Collectors.toList())

    return JiraProject(
        key = mapping.boardProjects[name],
        name = name,
        type = mapping.projectType,
        description = desc,
        issues = issues
    )
}

data class DataMapping(
    val projectType: String,
    val boardProjects: HashMap<String, String>,
    val listStatus: HashMap<String, String>,
    val archiveStatus: String,
    val users: HashMap<String, String>,
    val labels: HashMap<String, String>,
    val labelsTypes: HashMap<String, String>,
    val stateStatus: HashMap<String, String>
)

class Transform : CliktCommand("Generate Jira JSONs from Trello JSONs") {

    private val verbose by option().flag("--no-verbose")
    private val inputDirectory: File by option("-i", "--input", help = "Directory with JSONs used as input").file().prompt("Input directory")
    private val outputDirectory: File by option("-o","--output", help = "Directory new JSONs will be generated as output").file().prompt("Output directory")
    private val mappingFile: File by option("-m", "--mapping", help = "Mapping file relations entities and values between the two systems").file().prompt("Mapping file")

    private val DEFAULT_EXTENSION = "*.json"
    private val gson = GsonBuilder()
        .registerTypeAdapter(TrelloBoard::class.java, TrelloBoardJsonDeserializer())
        .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeStringSerializer())
        .setPrettyPrinting()
        .create()

    override fun run() {
        echo("Generating Jira JSONs...")
        if (verbose) echo("Finding $DEFAULT_EXTENSION in dir: ${inputDirectory.absolutePath}")

        val jsonsFiles = getAllFilesThatMatchExtension(inputDirectory, DEFAULT_EXTENSION)
        val mapping: DataMapping = gson.fromJson(JsonReader(FileReader(mappingFile)), DataMapping::class.java)

        jsonsFiles.parallelStream().forEach { f ->
            val trelloBoard: TrelloBoard = gson.fromJson(JsonReader(FileReader(f)), TrelloBoard::class.java)
            val output = gson.toJson(trelloBoard.toJira(mapping))
            saveFile(f.name, output)
        }

        echo("Done!")
    }

    private fun saveFile(filename: String, json: String) {
        val f = File(outputDirectory, filename)
        f.writeText(json)
        if (verbose) echo("Saved file in: ${f.absolutePath}")
    }
}
