package commands

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import com.github.ajalt.clikt.parameters.types.file
import java.io.File
import java.util.*
import java.util.stream.Stream
import getAllFilesThatMatchExtension

class Authors : CliktCommand("Generate authors mapping file") {

    private val verbose by option().flag("--no-verbose")
    private val inputDirectory: File by option("-i", "--input", help="Directory with JSONs used as input").file().prompt("Input directory")

    private val DEFAULT_AUTHORS_PATH = "authors.txt"
    private val DEFAULT_EXTENSION = "*.json"

    override fun run() {
        echo("Verbose mode is ${if (verbose) "on" else "off"}")
        echo("Generating authors mapping...")

        if (verbose) echo("Finding $DEFAULT_EXTENSION in dir: ${inputDirectory.absolutePath}")

        val jsons = getAllFilesThatMatchExtension(inputDirectory, DEFAULT_EXTENSION)
        val authors = jsons.findAuthors()
        saveFile(authors)
    }

    private fun Collection<File>.findAuthors(): Stream<String> =
        this.parallelStream().flatMap { f ->
            if (verbose) echo("Finding authors in: ${f.absolutePath}")
            val reader = Scanner(f)
            val authors = findAllUsers(reader)
            authors
        }.distinct().sorted()

    private fun findAllUsers(s: Scanner): Stream<String> = s
        .findAll("username\":\\s*\"([^\"]+)")
        .map { it.group(1) }
        .distinct()

    private fun saveFile(authors: Stream<String>) {
        val f = File(DEFAULT_AUTHORS_PATH)
        f.printWriter().use { out -> authors.forEach { out.println("$it = ") } }
        echo("Saved file in ${f.absolutePath}")
    }
}

