package deserializers

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import domain.models.*
import java.lang.reflect.Type
import java.time.ZonedDateTime

class TrelloBoardJsonDeserializer : JsonDeserializer<TrelloBoard> {
    private val gson = Gson()

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): TrelloBoard {
        val root = json.asJsonObject
        val lists = deserializeLists(root.getAsJsonArray("lists"))
        val members = deserializeMembers(root.getAsJsonArray("members"))
        val actions = deserializeActions(root.getAsJsonArray("actions"), members)
        val labels = deserializeLabels(root.getAsJsonArray("labels"))
        val checklists = deserializeChecklists(root.getAsJsonArray("checklists"))
        val cards = deserializeCards(root.getAsJsonArray("cards"), lists, members, labels, actions, checklists)

        return TrelloBoard(
            id = root.getAsJsonPrimitive("id").asString,
            name = root.getAsJsonPrimitive("name").asString,
            desc = root.getAsJsonPrimitive("desc").asString,
            shortUrl = root.getAsJsonPrimitive("shortUrl").asString,
            dateLastActivity = ZonedDateTime.parse(root.getAsJsonPrimitive("dateLastActivity").asString),
            labelNames = gson.fromJson(
                root.getAsJsonObject("labelNames"),
                object : TypeToken<HashMap<String, String>>() {}.type
            ),
            cards = cards,
            lists = lists,
            members = members,
            actions = actions,
            checklists = checklists
        )
    }

    private fun deserializeChecklists(checklists: JsonArray) =
        checklists
            .map { it.asJsonObject }
            .map {
                TrelloChecklist(
                    id = it.getAsJsonPrimitive("id").asString,
                    name = it.getAsJsonPrimitive("name").asString,
                    idCard = it.getAsJsonPrimitive("idCard").asString,
                    checkItems = it.getAsJsonArray("checkItems").asJsonArray
                        .map { i -> i.asJsonObject }
                        .map { i ->
                            TrelloChecklistItem(
                                id = i.getAsJsonPrimitive("id").asString,
                                state = i.getAsJsonPrimitive("state").asString,
                                name = i.getAsJsonPrimitive("name").asString
                            )
                        }
                )
            }

    /**
     * Just deserialize Comments. Other activity is omitted.
     */
    private fun deserializeActions(actions: JsonArray, members: Collection<TrelloMember>): Collection<TrelloAction> =
        actions
            .map { it.asJsonObject }
            .filter { it.getAsJsonPrimitive("type").asString == "commentCard" } // TODO This should deserialize all actions
            .map {
                val data = it.getAsJsonObject("data")
                TrelloAction(
                    id = it.getAsJsonPrimitive("id").asString,
                    creator = members.find { m -> m.id == it.getAsJsonPrimitive("idMemberCreator").asString },
                    type = it.getAsJsonPrimitive("type").asString,
                    date = ZonedDateTime.parse(it.getAsJsonPrimitive("date").asString),
                    data = TrelloActionData(
                        text = data.getAsJsonPrimitive("text").asString,
                        cardId = data.getAsJsonObject("card").getAsJsonPrimitive("id").asString
                    )
                )
            }

    private fun deserializeLists(lists: JsonArray): Collection<TrelloList> =
        gson.fromJson(lists, object : TypeToken<Collection<TrelloList>>() {}.type)

    private fun deserializeMembers(members: JsonArray): Collection<TrelloMember> =
        gson.fromJson(members, object : TypeToken<Collection<TrelloMember>>() {}.type)

    private fun deserializeLabels(labels: JsonArray): Collection<TrelloLabel> =
        gson.fromJson(labels, object : TypeToken<Collection<TrelloLabel>>() {}.type)

    private fun deserializeAttachments(
        attachments: JsonArray,
        members: Collection<TrelloMember>
    ): Collection<TrelloAttachment> =
        attachments
            .map { it.asJsonObject }
            .map {
                TrelloAttachment(
                    date = ZonedDateTime.parse(it.getAsJsonPrimitive("date").asString),
                    member = members.find { m -> m.id == it.getAsJsonPrimitive("idMember").asString },
                    isUpload = it.getAsJsonPrimitive("isUpload").asBoolean,
                    name = it.getAsJsonPrimitive("name").asString,
                    url = it.getAsJsonPrimitive("url").asString
                )
            }

    private fun deserializeCards(
        cards: JsonArray,
        lists: Collection<TrelloList>,
        members: Collection<TrelloMember>,
        labels: Collection<TrelloLabel>,
        actions: Collection<TrelloAction>,
        checklists: Collection<TrelloChecklist>
    ): Collection<TrelloCard> =
        cards
            .map { it.asJsonObject }
            .map {
                val m: Collection<TrelloMember> =
                    it.getAsJsonArray("idMembers").map { s -> TrelloMember(id = s.asString) }
                val l: Collection<TrelloLabel> = it.getAsJsonArray("idLabels").map { s -> TrelloLabel(id = s.asString) }
                val id = it.getAsJsonPrimitive("id").asString

                TrelloCard(
                    id = id,
                    name = it.getAsJsonPrimitive("name").asString,
                    desc = it.getAsJsonPrimitive("desc").asString,
                    closed = it.getAsJsonPrimitive("closed").asBoolean,
                    dateLastActivity = ZonedDateTime.parse(it.getAsJsonPrimitive("dateLastActivity").asString),
                    list = lists.find { list -> list.id == it.getAsJsonPrimitive("idList").asString },
                    dueComplete = it.getAsJsonPrimitive("dueComplete").asBoolean,
                    shortUrl = it.getAsJsonPrimitive("shortUrl").asString,
                    members = members.intersect(m),
                    attachments = deserializeAttachments(it.getAsJsonArray("attachments"), members),
                    labels = labels.intersect(l),
                    actions = actions.filter { a -> a.data?.cardId == id },
                    checklists = checklists.filter { list -> list.idCard == id }
                )
            }
}