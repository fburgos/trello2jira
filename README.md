# Trello2Jira
Transform Trello JSONs into Jira JSONs so that you can import in Jira using Jira JSON importer

## System requirements
```shell script
  openjdk version "11.0.7" 2020-04-14
  OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.7+10)
  Eclipse OpenJ9 VM AdoptOpenJDK (build openj9-0.20.0, JRE 11 Windows 10 amd64-64-Bit Compressed References 20200422_551 (JIT enabled, AOT enabled)
  OpenJ9   - 05fa2d361
  OMR      - d4365f371
  JCL      - 838028fc9d based on jdk-11.0.7+10)
```

## Basic usage
1. Export JSONs from Trello Boards and save them in a directory
2. Run transformation:
```shell script
$ java -jar Trello2Jira-1.0.0-all.jar transform -i ./trello_jsons -o ./jira_jsons -m ./mapping_trello_jira.json --verbose
``` 
3. Review Jira JSONs were generated in the output directory
4. Use Jira JSON importer to upload them to Jira.

### Commands
- Display cli help:
```shell script
$ java -jar Trello2Jira-1.0.0-all.jar --help
```
- Extract card users from Trello JSON for mapping users with Jira
```shell script
$ java -jar Trello2Jira-1.0.0-all.jar -a 
```