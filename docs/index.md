# Trello2Jira
Move on from Trello to Jira painless

---
## Overview
Trello2Jira is a **fast**, **simple** and **painless** way to transforms your Trello boards cards into Jira issues.
This CLI allow you point to an input directory and a mapping file with relations between Trello and Jira and you will get an output JSON with the Jira format expected by the [Jira JSON importer](https://confluence.atlassian.com/adminjiraserver/importing-data-from-json-938847609.html). 

## Installation
- [Download](https://gitlab.com/fburgos/trello2jira/-/jobs/artifacts/develop/download?job=publish) latest binary or [clone](https://gitlab.com/fburgos/trello2jira) and build the source repository
- JDK 11 is required. You can [download OpenJDK](https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=openj9) if you have not already installed:
```shell
  openjdk version "11.0.7" 2020-04-14
  OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.7+10)
  Eclipse OpenJ9 VM AdoptOpenJDK (build openj9-0.20.0, JRE 11 Windows 10 amd64-64-Bit Compressed References 20200422_551 (JIT enabled, AOT enabled)
  OpenJ9   - 05fa2d361
  OMR      - d4365f371
  JCL      - 838028fc9d based on jdk-11.0.7+10)
```

## Basic usage 
- Export [JSONs from Trello Boards](https://help.trello.com/article/747-exporting-data-from-trello-1) and save them in a directory
- Create a file with the mapping between Trello and Jira. Eg: *mapping_trello_jira.json*

```json
{
  "projectType": "software",
  "boardProjects": {
    "Welcome Board": "WB"
  },
  "listStatus": {
    "Basics": "Open",
    "Intermediate": "In Progress",
    "Advanced": "Done"
  },
  "archiveStatus": "Done",
  "users": {
    "batman": "bruce_thomas",
    "superman69": "clark_kent"
  },
  "labels": {
    "green": "Easy",
    "orange": "Medium",
    "red": "Complex"
  },
  "labelsTypes": {
    "requirement": "story",
    "incident": "bug"
  },
  "stateStatus": {
    "incomplete": "open",
    "complete": "done"
  }
}
``` 
- Run the transformation:
```shell
$ java -jar Trello2Jira-1.0.0-all.jar transform -i ./trello_jsons -o ./jira_jsons -m ./mapping_trello_jira.json --verbose
``` 
- Review Jira JSONs were generated in the output directory
- Use [Jira JSON importer](https://confluence.atlassian.com/adminjiraserver/importing-data-from-json-938847609.html) to upload them to Jira.
- If you are happy with the result then click on "Donate" button :)

## Commands
- `$ java -jar Trello2Jira-1.0.0-all.jar --help` - Display cli help
- `$ java -jar Trello2Jira-1.0.0-all.jar -a` - Extract Trello users
- `$ java -jar Trello2Jira-1.0.0-all.jar transform -i ./trello_jsons -o ./jira_jsons -m ./mapping_trello_jira.json` - Transform Trello JSONs to Jira JSON